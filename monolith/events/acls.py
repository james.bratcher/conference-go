from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests,json 

def get_picture(city, state):
    url = f'https://api.pexels.com/v1/search?query={city}&{state}?per_page=1?page=1'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    content = json.loads(response.text)
    return {
        "picture_url": content["photos"][0]["src"]["original"]
    }

def get_weather(city, state):
    # Create the URL for the geocoding API with the city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    geo_response = requests.get(geo_url, headers=headers)
    geo_data = json.loads(geo_response.content)
    if len(geo_data) > 0 and "lat" in geo_data[0] and "lon" in geo_data[0]:
        lat = geo_data[0]["lat"]
        lon = geo_data[0]["lon"]
    else:
        return "null"
    
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    weather_response = requests.get(weather_url, headers=headers)
    weather_data = json.loads(weather_response.content)

    temp = weather_data["main"]["temp"]
    desc = weather_data["weather"][0]["description"]
    weather_dict = {"temperature": temp, "description": desc}
    return weather_dict

    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary